# Основы работы с потоками в C#
## Цель работы
Получить базовые знания и навыки работы с потоками в языке программирования С#. Организовать простую многопоточную работу приложения с последующим анализом результатов.

## Практическая часть
1) Ввести одномерный массив с количеством элементов N
2) Обработать каждый элемент массива следующим способом:
```cpp
for(int i=0; i<a.Length; i++) {
  for(int j=0; j < K; j++)
    b[i] += Math.Pow(a[i], 1.789);
}
// где а – вводимый массив, К – параметр сложности.
```
3) Определить время обработки без использования многопоточности.
4) Определить время обработки с использованием многопоточности.
5) Сравнить результаты.