﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Lab3
{
    class Program
    {
        static Random rand = new Random();

        static void Main(string[] args)
        {
            int size;
            do
            {
                Console.Write("Input size of massiv: ");
            } while (!int.TryParse(Console.ReadLine(), out size));

            int count;
            do
            {
                Console.Write("Input size of massiv: ");
            } while (!int.TryParse(Console.ReadLine(), out count));

            int[] mas = Generate(size);

            Stopwatch stopwatch = new Stopwatch();

            stopwatch.Start();
            int[] mas1 = new int[size];
            for (int i = 0; i < mas1.Length; i++)
            {
                for (int j = 0; j < count; j++)
                {
                    mas1[i] += (int)Math.Pow(mas[i], 1.789);
                }
            }
            stopwatch.Stop();

            Console.WriteLine("Time: {0}", stopwatch.ElapsedTicks);

            stopwatch.Reset();
            stopwatch.Start();
            int[] mas2 = new int[size];
            for (int i = 0; i < mas2.Length - 1; i++)
            {
                Thread thread = new Thread(() =>
                {
                    for (int j = 0; j < count / 2; j++)
                    {
                        mas2[i] += (int)Math.Pow(mas[i], 1.789);
                    }
                });
                thread.Start();

                for (int j = count / 2; j < count; j++)
                {
                    mas2[i] += (int)Math.Pow(mas[i], 1.789);
                }
            }
            stopwatch.Stop();
            Console.WriteLine("Time: {0}", stopwatch.ElapsedTicks);
            Print(mas1);
            Print(mas2);
        }

        static int[] Generate(int size)
        {
            int[] mas = new int[size];

            for (int i = 0; i < mas.Length; i++)
            {
                mas[i] = rand.Next(1, 10);
            }

            return mas;
        }

        static void Print(int[] mas)
        {
            foreach (var item in mas)
            {
                Console.Write("{0} ", item);
                
            }
            Console.WriteLine();
        }
    }
}
